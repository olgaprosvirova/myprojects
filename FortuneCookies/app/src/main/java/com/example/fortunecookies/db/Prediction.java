package com.example.fortunecookies.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.fortunecookies.toolsAndConstants.AppConstants;

public class Prediction {
    private int m_nId;
    private String m_strEng = "";
    private String m_strRus = "";

    public Prediction(){

    }

    public Prediction( String strEng, String strRus) {
        this.m_strEng = strEng;
        this.m_strRus = strRus;
    }

    public Prediction(Cursor cursor){
        this.m_nId = cursor.getInt(cursor.getColumnIndex(AppConstants.FIELD_PREDICTIONS_ID));
        this.m_strEng = cursor.getString(cursor.getColumnIndex(AppConstants.FIELD_PREDICTIONS_ENG));
        this.m_strRus = cursor.getString(cursor.getColumnIndex(AppConstants.FIELD_PREDICTIONS_RUS));

    }

    public int getId() {
       return this.m_nId;
    }

    public void setId(int m_nId) {
         this.m_nId = m_nId;
}
    public String getEng() {
        return m_strEng;
    }

    public void setEng(String m_strEng) {
        this.m_strEng = m_strEng;
    }

    public String getRus() {
        return m_strRus;
    }

    public void setRus(String m_strRus) {
        this.m_strRus = m_strRus;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(AppConstants.FIELD_PREDICTIONS_ENG, getEng());
        values.put(AppConstants.FIELD_PREDICTIONS_RUS, getRus());
        return values;
    }
}
