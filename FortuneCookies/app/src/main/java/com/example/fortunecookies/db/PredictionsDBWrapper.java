package com.example.fortunecookies.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.fortunecookies.toolsAndConstants.AppConstants;

import java.util.ArrayList;

public class PredictionsDBWrapper {
    private DBHelper m_dbHelper = null;
    private String m_strTableName = "";

    public PredictionsDBWrapper(Context context){
        m_dbHelper = new DBHelper(context);
        m_strTableName = AppConstants.TABLE_NAME_PREDICTIONS_TABLE;

    }

    public Prediction getPredictionById(int nId){
        Prediction result = null;
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        String strSelect = AppConstants.FIELD_PREDICTIONS_ID + "=?";
        Log.i("db", strSelect);
        String[] arrArg = new String[]{Integer.toString(nId)};

        Log.i("db", arrArg[0]);
        Cursor cursor = db.query(m_strTableName,null,strSelect,arrArg,null,null,null);
        if (cursor!=null){
            Log.i("db", "not null");
            if(cursor.moveToFirst()){
                Log.i("db", "moved to position");
                result = new Prediction(cursor);
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public long insertPrediction(Prediction item){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        long nId = db.insert(m_strTableName,null,item.getContentValues());
        db.close();
        return nId;
    }

    public int getPredictionCount(){
        ArrayList<Prediction> arrResult = new ArrayList<>();
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        Cursor cursor = db.query(m_strTableName,null,null,null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                do {
                    arrResult.add(new Prediction(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult.size();
    }

    public boolean removePrediction(Prediction item) {
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        String strSelect = AppConstants.FIELD_PREDICTIONS_ID + "=?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        int nCountUdate = db.delete(m_strTableName,strSelect, arrArg);
        db.close();
        return nCountUdate==1;
    }

    public boolean removeAll() {
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        int nCountUdate = db.delete(m_strTableName,null, null);
        db.close();
        return nCountUdate>0;
    }

    public ArrayList<Prediction> getAll() {
        ArrayList<Prediction> arrResult = new ArrayList<>();
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        Cursor cursor = db.query(m_strTableName,null,null,null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                do {
                    arrResult.add(new Prediction(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }

   /* public Prediction getRandomPrediction(){
        Prediction random=new Prediction();
        random=getPredictionById(new Double(Math.random()*getPredictionCount()).longValue());
        Log.i("database", " "+random.getId());
        return random;
    }*/


}
