package com.example.fortunecookies.toolsAndConstants;


import android.content.Context;
import android.content.SharedPreferences;

public class AppSettings {



    private SharedPreferences m_SharedPreferences = null;

    public  AppSettings (Context context){
        m_SharedPreferences = context.getSharedPreferences(AppConstants.PREFS, 0);
    }


    public boolean isFirstStart(){
        return m_SharedPreferences.getBoolean(AppConstants.IS_FIRST_START, true);
    }

    public void setIsFirstStart(boolean bIsFirstStart){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(AppConstants.IS_FIRST_START, bIsFirstStart);
        editor.apply();
    }

    public void setLanguage(int language_code){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putInt(AppConstants.LANGUAGE, language_code);
        editor.apply();

    }

    public int getLanguage(){
                return m_SharedPreferences.getInt(AppConstants.LANGUAGE, 0);
    }
}
