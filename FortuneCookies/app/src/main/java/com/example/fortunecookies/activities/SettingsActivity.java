package com.example.fortunecookies.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import com.example.fortunecookies.R;
import com.example.fortunecookies.toolsAndConstants.AppConstants;
import com.example.fortunecookies.toolsAndConstants.AppSettings;
import com.yariksoffice.lingver.Lingver;


public class SettingsActivity extends Activity implements View.OnClickListener {

    private AppSettings m_Settings;
    private int m_languageCode=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        m_Settings=new AppSettings(this);

        View buttonOk=findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(this);

        RadioGroup radioGroup=findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                                      View checked = findViewById(radioGroup.getCheckedRadioButtonId());
                                                      checked.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                                      switch (i){
                                                          case R.id.radioButton1: {
                                                              setLanguageCode(i);
                                                              findViewById(R.id.radioButton2).setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                                              Lingver.getInstance().setLocale(SettingsActivity.super.getBaseContext(), AppConstants.STRING_EN);
                                                              break;
                                                          }
                                                          case R.id.radioButton2: {
                                                              setLanguageCode(i);
                                                              findViewById(R.id.radioButton1).setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                                              Lingver.getInstance().setLocale(SettingsActivity.super.getBaseContext(), AppConstants.STRING_RU);
                                                              break;
                                                          }
                                                      }
                                                  }
                                              }
        );

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
              case R.id.buttonOk:
            {
                m_Settings.setIsFirstStart(false);
                m_Settings.setLanguage(getLanguageCode());
                super.finish();
            }
        }

    }

    public void setLanguageCode(int languageCode) {
        this.m_languageCode = languageCode;
    }

    public int getLanguageCode(){
        return this.m_languageCode;
    }


}
