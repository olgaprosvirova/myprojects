package com.example.fortunecookies.toolsAndConstants;

public class AppConstants {

        // DB Constants
        public static final String DB_NAME = "db_predictions";
        public static final int DB_VERSION = 1;

        public static final String TABLE_NAME_PREDICTIONS_TABLE = "_PREDICTIONS_TABLE";
        public static final String FIELD_PREDICTIONS_ID = "rowid";
        public static final String FIELD_PREDICTIONS_ENG = "_prediction_eng";
        public static final String FIELD_PREDICTIONS_RUS = "_prediction_rus";

        // Activities Constants

        public static final int REQUEST_CODE_SETTINGS_ACTIVITY = 1234;

        // Settings Constants

        public final static String PREFS = "PREFS";
        public final static String LANGUAGE = "LANGUAGE";
        public final static String IS_FIRST_START = "IS_FIRST_START";

        // Languages Constants
        public static final String STRING_EN = "en";
        public static final String STRING_RU = "ru";
}
