package com.example.fortunecookies.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.fortunecookies.toolsAndConstants.AppConstants;

public class DBHelper extends SQLiteOpenHelper {



    public DBHelper(Context context) {
        super(context, AppConstants.DB_NAME, null, AppConstants.DB_VERSION);


    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstants.TABLE_NAME_PREDICTIONS_TABLE +
                " (" + AppConstants.FIELD_PREDICTIONS_ID + " INTEGER PRIMARY KEY, " +
                AppConstants.FIELD_PREDICTIONS_ENG + " TEXT NOT NULL, " +
                AppConstants.FIELD_PREDICTIONS_RUS + " TEXT NOT NULL);");
            }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ AppConstants.TABLE_NAME_PREDICTIONS_TABLE);
        onCreate(db);
    }


}
