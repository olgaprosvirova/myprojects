package com.example.fortunecookies.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import com.example.fortunecookies.db.PredictionEngine;
import com.example.fortunecookies.R;
import com.example.fortunecookies.toolsAndConstants.AppConstants;
import com.example.fortunecookies.toolsAndConstants.AppSettings;
import com.yariksoffice.lingver.Lingver;


public class MainActivity extends AppCompatActivity {

    private PredictionEngine engine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings settings = new AppSettings(this);

        engine = new PredictionEngine(this);


        if (settings.isFirstStart()){
            engine.fiLLDB();
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, AppConstants.REQUEST_CODE_SETTINGS_ACTIVITY);
            settings.setIsFirstStart(false);
        }


        final ImageButton imageButton=findViewById(R.id.imageButton);
        imageButton.setImageResource(R.drawable.cookie1);
        imageButton.setTag(R.drawable.cookie1);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog fortuneDialog=new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogTheme).create();
                switch (Lingver.getInstance().getLanguage()){
                    case AppConstants.STRING_EN:{
                        fortuneDialog.setMessage(engine.getRandomPrediction().getEng());
                        break;
                    }
                    case AppConstants.STRING_RU: {
                        fortuneDialog.setMessage(engine.getRandomPrediction().getRus());
                        break;
                    }
                }
                fortuneDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface fortuneDialog, int which) {
                               fortuneDialog.dismiss();
                            }
                        });
                fortuneDialog.show();
                if ((Integer) imageButton.getTag()==R.drawable.cookie1){
                    imageButton.setImageResource(R.drawable.cookie2);
                    imageButton.setTag(R.drawable.cookie2);
                }
                else {
                    imageButton.setImageResource(R.drawable.cookie1);
                    imageButton.setTag(R.drawable.cookie1);
                }
            }
        });

        ImageButton settingsButton=findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivityForResult(intent, AppConstants.REQUEST_CODE_SETTINGS_ACTIVITY);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        super.recreate();
    }

}
