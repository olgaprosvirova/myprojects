package com.example.fortunecookies;

import android.app.Application;
import com.example.fortunecookies.toolsAndConstants.AppConstants;
import com.yariksoffice.lingver.Lingver;

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Lingver.init(this, AppConstants.STRING_EN);
    }

}
