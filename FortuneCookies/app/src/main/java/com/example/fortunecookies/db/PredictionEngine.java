package com.example.fortunecookies.db;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

public class PredictionEngine {

    private Context m_Context = null;

    public PredictionEngine(Context context){
        m_Context  = context;
    }


    public ArrayList<Prediction> getAll(){
        PredictionsDBWrapper dbWrapper = new PredictionsDBWrapper(m_Context);
        ArrayList<Prediction> arrData = dbWrapper.getAll();
        return arrData;
    }

    public int getCount(){
        PredictionsDBWrapper dbWrapper = new PredictionsDBWrapper(m_Context);
        ArrayList<Prediction> arrData = dbWrapper.getAll();
        return arrData.size();
    }


    public Prediction getPredictionById(int nId){
        PredictionsDBWrapper dbWrapper = new PredictionsDBWrapper(m_Context);
        Prediction item = dbWrapper.getPredictionById(nId);
        return item;
    }

    public boolean insertPrediction(Prediction item){
        PredictionsDBWrapper dbWrapper = new PredictionsDBWrapper(m_Context);
        long nId = dbWrapper.insertPrediction(item);
        return nId>=0;
    }

   public boolean removePrediction(Prediction item){
        PredictionsDBWrapper dbWrapper = new PredictionsDBWrapper(m_Context);
        return dbWrapper.removePrediction(item);
    }

    public boolean removeAll(){

        PredictionsDBWrapper dbWrapper = new PredictionsDBWrapper(m_Context);
        return dbWrapper.removeAll();

    }

        public void fiLLDB(){
        insertPrediction(new Prediction("Help! I'm trapped in a cookie factory!", "Помогите! Меня удерживают на фабрике печенья!"));
        insertPrediction(new Prediction("Be yourself; everyone else is already taken", "Когда, совершив ошибку, не исправил ее, это и называется совершить ошибку"));
        insertPrediction(new Prediction("If you tell the truth, you don't have to remember anything", "Начинать всегда стоит с того, что сеет сомнения"));
        insertPrediction(new Prediction("A friend is someone who knows all about you and still loves you", "Если вы думаете, что на что-то способны, вы правы; если думаете, что у вас ничего не получится - вы тоже правы"));
        insertPrediction(new Prediction("We accept the love we think we deserve", "Лучше быть уверенным в хорошем результате, чем надеяться на отличный"));
        insertPrediction(new Prediction("You only live once, but if you do it right, once is enough", "Определенность цели - отправная точка всех достижений"));
        insertPrediction(new Prediction("Time you enjoy wasting is not wasted time", "Календари не имеют соперников в искусстве предсказывать будущее"));
        insertPrediction(new Prediction("Sometimes your joy is the source of your smile, but sometimes your smile can be the source of your joy", "Дальновидность меряется результатами"));
        insertPrediction(new Prediction("Life is really simple, but we insist on making it complicated", "Мы доказываем при помощи логики, но открываем благодаря интуиции"));
        insertPrediction(new Prediction("The more one judges, the less one loves", "Подумай то же самое, но иначе"));

            insertPrediction(new Prediction("For success, attitude is equally as important as ability", "Подумай то же самое, но иначе"));
            insertPrediction(new Prediction("Doing the best at this moment puts you in the best place for the next moment", "Вы скоро проголодаетесь: виртуальным печеньем не наешься"));
            insertPrediction(new Prediction("What seems to us as bitter trials are often blessings in disguise", "Если двое читают одну книгу, это не одна и та же книга"));
            insertPrediction(new Prediction("Don't let what you cannot do interfere with what you can do", "Нелепо было бы требовать, чтобы одно и то же дерево одновременно приносило цветы и плоды"));
            insertPrediction(new Prediction("The road to success is always under construction", "Настоящее приключение отличить легко – пока оно происходит, тебе чаще всего совершенно не весело"));
            insertPrediction(new Prediction("Success is going from failure to failure without losing enthusiasm", "Неудобство – это лишь неверно воспринятое приключение, приключение же – верно воспринятое неудобство"));
            insertPrediction(new Prediction("Loving yourself isn’t vanity. It’s sanity", "Напрасно искать кнопку выключения на кошке, которая желает позавтракать"));
            insertPrediction(new Prediction("It ain't what they call you, it's what you answer to", "Если бы кошки умели говорить, они бы все равно молчали"));
            insertPrediction(new Prediction("What lies behind us and what lies before us are tiny matters compared to what lies within us", "Бессознательному, чтобы обнаружить себя, требуется время"));
            insertPrediction(new Prediction("Time moves in one direction, memory in another", "По-настоящему понимаешь лишь то, что можешь объяснить своей бабушке"));

            insertPrediction(new Prediction("Lose an hour in the morning, and you will spend all day looking for it", "Тот, кто старается все предвидеть, теряет бдительность"));
            insertPrediction(new Prediction("You cannot explore the universe if you think that you are the center of it", "Весна приходит необычно рано или необычно поздно — это дело обычное"));
            insertPrediction(new Prediction("Wherever you go, go with all your heart", "Острословие обнаруживает в человеке острый язык, но не обязательно острый ум"));
            insertPrediction(new Prediction("Look deep into nature, and then you will understand everything better", "Вечность не есть сумма времени"));
            insertPrediction(new Prediction("On earth there is no heaven, but there are pieces of it", "О вкусах не спорят: из за вкусов скандалят и ругаются"));
            insertPrediction(new Prediction("If you are too busy to laugh, you are too busy", "Ключевой ингредиент успеха в незнании того, что ваши намерения невозможно осуществить"));
            insertPrediction(new Prediction("A warm smile is the universal language of kindness", "Фантазия – велотренажер для ума. Вы никуда не уйдете, зато подкачаете мышцы, которые помогут ехать дальше"));
            insertPrediction(new Prediction("Childhood means simplicity. Look at the world with the child's eye — it is very beautiful", "Общество тех, кто постоянно ищет правду, предпочтительнее тех, кто считает, что уже нашел ее"));
            insertPrediction(new Prediction("Help! I'm trapped in a cookie factory!", "Даже если вы знаете, как все устроено, это все равно магия"));
            insertPrediction(new Prediction("Aim for the moon. If you miss, you may hit a star", "Хватит повторять старые ошибки, время совершать новые!"));

            insertPrediction(new Prediction("It always seems impossible until its done", "Когда мы до конца исследуем космос, окажется, что, будучи здесь, на земле, мы уже были в небе"));
            insertPrediction(new Prediction("If opportunity doesn't knock, build a door", "Космос вовсе не далек. До него всего час езды — при условии, что твоя машина может ехать вертикально вверх"));
            insertPrediction(new Prediction("Success is a journey not a destination", "Если вам надоест быть хозяином у себя дома, заведите кота"));
            insertPrediction(new Prediction("Every accomplishment starts with the decision to try", "Главное в дрессировке кошки — сделать вид, что ты отдал ей именно ту команду, которую она выполнила"));
            insertPrediction(new Prediction("Be there for others, but never leave yourself behind", "Если черная кошка перебежала вам дорогу, значит, она куда-то спешит"));
            insertPrediction(new Prediction("Be a first-rate version of yourself, instead of a second-rate version of somebody else", "Вся жизнь — управление рисками, а не исключение рисков"));
            insertPrediction(new Prediction("With the new day comes new strength and new thoughts", "Все что ты говоришь, говорит о тебе"));
            insertPrediction(new Prediction("You don't always need a plan", "Никогда не выражайся чётче, чем способен мыслить"));
            insertPrediction(new Prediction("Despite the forecast, live like it's spring", "Вы скоро проголодаетесь: виртуальным печеньем не наешься"));
            insertPrediction(new Prediction("Help! I'm trapped in a cookie factory!", "Помогите! Меня удерживают на фабрике печенья!"));
        Log.i("db count", " "+this.getCount());
    }

    public Prediction getRandomPrediction(){
        PredictionsDBWrapper dbWrapper = new PredictionsDBWrapper(m_Context);
        int random=(int)(this.getCount()*Math.random());
        if (random==0){
            random++;
        }
        Prediction item = dbWrapper.getPredictionById(random);
        return item;
    }

}
